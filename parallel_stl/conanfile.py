#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#
import os

from conans import CMake, \
                   tools

from conans.model.conan_file import ConanFile
from conans.errors import ConanInvalidConfiguration


class ParallelSTL(ConanFile):
    name = "parallel_stl"
    version = "20181204"
    license = "Apache-2.0"
    url = "https://bitbucket.org/headcount_bimandscan/conan-intel-parallel-stl"
    description = "Intel's parallel STL implementation."
    author = "Neil Hyland <neil.hyland@bimandscan.com>"
    generators = "cmake"
    homepage = "https://github.com/intel/parallelstl"

    _src_dir = f"parallelstl-{version}"

    settings = "os", \
               "compiler", \
               "build_type", \
               "arch"

    exports = "../LICENCE.md"

    requires = "tbb/2019_U2@bimandscan/stable"

    def _valid_cppstd(self):
        return self.settings.compiler.cppstd == "17" or \
               self.settings.compiler.cppstd == "gnu17" or \
               self.settings.compiler.cppstd == "20" or \
               self.settings.compiler.cppstd == "gnu20" or \
               self.settings.compiler.cppstd == "11" or \
               self.settings.compiler.cppstd == "gnu11" or \
               self.settings.compiler.cppstd == "14" or \
               self.settings.compiler.cppstd == "gnu14"

    def config_options(self):
        if not self._valid_cppstd():
            raise ConanInvalidConfiguration("Library requires C++11 or higher!")

    def source(self):
        zip_name = f"{self._src_dir}.zip"
        src_cmakefile = f"{self._src_dir}/CMakeLists.txt"

        tools.download(f"https://github.com/intel/parallelstl/archive/{self.version}.zip",
                       zip_name)

        tools.unzip(zip_name)
        os.unlink(zip_name)

        tools.replace_in_file(src_cmakefile,
                              "project(ParallelSTL VERSION ${VERSION_MAJOR}.${VERSION_MINOR} LANGUAGES CXX)",
                              "project(ParallelSTL VERSION ${VERSION_MAJOR}.${VERSION_MINOR} LANGUAGES CXX)\ninclude(\"${CMAKE_BINARY_DIR}/conanbuildinfo.cmake\")\nconan_basic_setup(TARGETS)\n")

    def configure_cmake(self):
        cmake = CMake(self)

        # Configure CMake library build:
        cmake.definitions["CMAKE_INSTALL_PREFIX"] = self.package_folder
        cmake.definitions["PARALLELSTL_USE_PARALLEL_POLICIES"] = True
        cmake.definitions["PARALLELSTL_BACKEND"] = "tbb"

        return cmake

    def build(self):
        cmake = self.configure_cmake()
        cmake.configure(source_folder = self._src_dir)
        cmake.build()

    def package(self):
        self.copy(pattern = f"ParallelSTL*.cmake",
                  src = ".",
                  dst = "lib/cmake/parallelstl",
                  keep_path = False)

        self.copy(pattern = "*",
                  src = f"{self._src_dir}/include",
                  dst = "include",
                  keep_path = True)

        self.copy("LICENSE",
                  "licenses",
                  self._src_dir)

    def package_info(self):
        self.cpp_info.libdirs = [
                                    "lib"
                                ]

        self.cpp_info.includedirs = [
                                        "include"
                                    ]

        if self.settings.compiler == "Visual Studio":
            self.cpp_info.cppflags.append("/openmp")
            self.cpp_info.cflags.append("/openmp")
        else:
            self.cpp_info.cppflags.append("-fopenmp")
            self.cpp_info.cflags.append("-fopenmp")

    def package_id(self):
        self.info.header_only()
