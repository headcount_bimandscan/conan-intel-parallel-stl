/*
 * 2018-2019 © BIM & Scan® Ltd.
 * See 'README.md' in the project root for more information.
 */
#include <cstdlib>
#include <iostream>
#include <vector>

#include <pstl/execution>
#include <pstl/algorithm>


int main(int p_arg_count,
         char** p_arg_vector)
{
    std::cout << "'Parallel STL' package test (compilation, linking, and execution).\n";

    std::vector<int> data(1000);

    std::fill_n(pstl::execution::par_unseq,
                data.begin(),
                data.size(),
                -1);

    std::cout << "'Parallel STL' package works!" << std::endl;
    return EXIT_SUCCESS;
}
