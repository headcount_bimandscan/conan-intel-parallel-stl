#!/bin/sh

#
# 2018-2019 � BIM & Scan� Ltd.
# See 'README.md' in the project root for more information.
#

set -e
conan upload -c -r "bimandscan-public" --all "parallel_stl/20181204@bimandscan/stable"
