# BIM & Scan� Third-Party Library (Intel Parallel STL)

![BIM & Scan](BimAndScan.png "BIM & Scan� Ltd.")

Conan build script for Intel's [parallel STL](https://github.com/intel/parallelstl) implementation.

Requires a C++11 compiler (or higher).

Supports version dated 4/12/2018 (stable).

Requires the [BIM & Scan� (public)](http://bsdev-jfrogartifactory.northeurope.cloudapp.azure.com/artifactory/webapp/) Conan repository for third-party dependencies.
