@ECHO OFF

REM
REM 2018-2019 © BIM & Scan® Ltd.
REM See 'README.md' in the project root for more information.
REM

CALL conan upload -c -r "bimandscan-public" --all "parallel_stl/20181204@bimandscan/stable"

@ECHO ON
